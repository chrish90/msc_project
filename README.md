# README #

This repository contains code related to the thesis **A Generative Approach to Learning for Ground-Truth Malware Classification**, submitted as part requirement for the MSc Degree in CSML at University College London.

Utilities and tools including the main algorithms for data processing, learning and evaluation can be found in `src`. The bulk of the analysis was done using iPython Notebooks, which can be found in `notebooks`.