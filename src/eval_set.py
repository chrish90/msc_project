import numpy as np
import pandas as pd
import re
import json

# list of AV regexes, some of the engines are omitted because they don't provide family descriptions
trend = '(?P<system>.+)_(?P<family>[A-Za-z]+)(?:\.(?P<variant>.*))?'
sym = '(?P<system>[A-Za-z0-9]+)\.(?P<family>[A-Za-z]+)(?:\.(?P<variant>[A-Za-z0-9]+))?'
kav = '((UDS|not-a-virus|HEUR):)*(?P<method>[A-Za-z0-9\-]+)\.(?P<system>[A-Za-z0-9\-]+)\.(?P<family>[A-Za-z]+)(?:\.(?P<variant>[A-Za-z0-9]+))?'
sophos ='((?P<system>.+)/)?(?P<family>[A-Za-z]+)(?:\-(?P<variant>.*))?'
mcafee ='((?P<system>.+)/)?(?P<family>[A-Za-z]+)(?:\.(?P<variant>.*))?( .*)?'
rising = '(?:(?P<method>(Trojan|Malware|Worm|Virus|Adware|Backdoor)\.)?(?P<system>Win32\.)(?P<family>[A-Za-z]+)[\.!](?P<variant>.+))?'
fprot = '(?P<system>.+)/(?P<family>[A-Za-z]+)(?:\.(?P<variant>.*))?'#'W32/Virut.AI!Generic (suspicious)',
format1 ='((Gen|Dropped|DeepScan):)?(?P<system>[A-Za-z0-9]+)\.(?P<family>[A-Za-z]+)(?:\.(?P<variant>[A-Za-z0-9]+))?'
format2 = '((?P<system>Win32|HEUR|Win-PUP|Win-Trojan)/(?P<family>[A-Za-z]+)(?:\.(?P<variant>.*))?|(?P<method>.*)/(?P<system1>.*)\.(?P<family1>.*))'#'Win-PUP/DomaIQ.Gen',
format3 = '(?P<system>.+)/(?P<family>[A-Za-z]+)(?:\.(?P<variant>.*))?'
format4 = '(?P<system>.+)/(?P<method>[A-Za-z]+)(?:\.(?P<family>.*))?'

av_re = {'trendmicro-housecall':'(?P<method>[A-Za-z]+)_(?P<family>[A-Za-z]+)\.(?P<variant>.+)',
         'zoner':'(?P<method>[A-Za-z]+)\.(?P<family>[A-Za-z]+)',
         'nprotect':'(?P<method>[A-Za-z]+)\.(?P<family>[A-Za-z]+)\.(?P<variant>.+)',
         'comodo':'(?P<method>[A-Za-z]+)\.(?P<system>.+)\.(?P<family>[A-Za-z]+)\.(?P<variant>.+)',
         'baidu':'(?P<system>[A-Za-z]+)\.(?P<method>.+)\.(?P<family>[A-Za-z]+)\.(?P<variant>.+)',
         'aegislab':'(?P<method>[A-Za-z]+)\.(?P<system>.+)\.(?P<family>[A-Za-z]+)',
         'thehacker': '(?P<method>[A-Za-z]+)/(?P<family>[A-Za-z]+)\.(?P<variant>.*)',
         'yandex': '(?P<method>[A-Za-z]+)\.(?P<family>[A-Za-z]+)!(?P<variant>.*)',
         'zillya': '(?P<method>[A-Za-z]+)\.(?P<family>[A-Za-z]+)\.(?P<system>.+)\.(?P<variant>.*)',
         'cat-quickheal':'(?P<method>[A-Za-z]+)\.(?P<family>[A-Za-z]+)\.(?P<variant>.*)',
         'malwarebytes':'(?P<method>[A-Za-z]+)\.(?P<family>[A-Za-z]+)',
         'virobot':'(?P<method>[A-Za-z]+)\.(?P<system>.+)\.(?P<family>[A-Za-z]+)\.(?P<variant>.*)',
         'avira':'(?P<method>[A-Za-z]+)/(?P<family>[A-Za-z]+)\.(?P<misc>[A-Za-z]+)\.(?P<variant>.*)',
         'esetnod32': '(?P<system>.[A-Za-z0-9]+)/(?:(?P<extra>[A-Za-z]+)\.)??(?P<family>[A-Za-z0-9]+)(?:\.(?P<variant>[A-Za-z0-9]+))?(\.gen)?( (?P<extra1>.+))?$',
         'microworld-escan':format1,
         'superantispyware':'(?P<method>.+)\.(?P<desc>[A-Za-z]+)/(?:Gen\-)?(?P<family>.*)',
         'ad-aware':format1,
         'ahnlab': format2,
         'jiangmin': sym,
         'antiy-avl': format4,
         'ahnlab-v3': format4,
         'eset-nod32': format3,
         'agnitum':'(?P<method>[A-Za-z]+).(?P<family>[A-Za-z]+)!(?P<variant>.*)',
         'qihoo-360': format2,
         'antivir': format3,
         'arcabit': format3,
         'vipre': rising,
         'trendmicro':  trend,
         'sophos_online':sophos,
         'symantec_beta': sym,
         'baidu-international':kav,
         'tencent':kav,
         'nano-antivirus':kav,
         'sunbelt': '(?P<method>(Email-Worm|LooksLike|BehavesLike|Trojan|Net-Worm|Virus|Trojan-Downloader|Backdoor|Worm|Adware)\.)?(?P<system>[A-Za-z0-9\-]+\.)?(?P<family>[A-Za-z]+)(?:\.(?P<variant>[A-Za-z0-9]+))?( [\([a-zA-Z\)]+)?',
         'trendmicro_consumer': trend,
         'rising': rising,
         'ikarus': kav,
         'fortinet': '(?P<system>.+)/(?P<family>[A-Za-z]+)(?:\.(?P<variant>.*))?',#'PUA/DomaIQ.Gen7',
         'avg': '(?:(?P<system>Win32|[A-Za-z0-9\-]+/))?(?P<family>[A-Za-z]+)(?:\.(?P<variant>.*))?',#'DomaIQ.AC (Adware)',
         'panda': '(?P<system>.+)/(?P<family>[A-Za-z]+)(?:\.(?P<variant>.*))?',#'PUA/DomaIQ.Gen7',
         'virusbuster': '(?P<system>[A-Za-z0-9]+)\.(?P<family>[A-Za-z]+)(?:\.(?P<variant>[A-Za-z0-9]+))?',
         'gdata': format1,
         'f-secure': format1,
         'alyac': format1,
         'emsisoft': format1,
         'symantec_online':  sym,
         'bitdefender': format1,
         'rising_online': rising,
         'mcafeegwedition_online': '(?P<system>[A-Za-z0-9]+)\.(?P<family>[A-Za-z]+)(?:\.(?P<variant>[A-Za-z0-9]+))?',
         'avast': '(?P<system>.+):(?P<family>[A-Za-z]+)(?:\:(?P<variant>.*))?',#'Win32:Sality',
         'drweb': '(?P<system>[A-Za-z0-9]+)\.(?P<family>[A-Za-z]+)(?:\.(?P<variant>[A-Za-z0-9]+))?',
         'symantec': sym,
         'command_online':'(?P<system>.+)/(?P<family>[A-Za-z]+)(?:\.(?P<variant>.*))?',#'W32/Virut.E.gen!Eldorado',
         'vba32': '(?P<system>[A-Za-z0-9\-]+)\.(?P<family>[A-Za-z]+)(?:\.(?P<variant>[A-Za-z0-9]+))?',
         'f_prot': fprot,
         'f-prot': fprot,
         'mcafee_online': mcafee,
         'clamav': '(?P<system>[A-Za-z0-9]+)\.(?P<family>[A-Za-z]+)(?:\.(?P<variant>[A-Za-z0-9]+))?',
         'ca_av': '(?P<system>.+)/(?P<family>[A-Za-z]+)(?:\.(?P<variant>.*))?',
         'kaspersky': kav,
         'mcafee': mcafee,
         'mcafee-gw-edition': mcafee,
         'k7computing': '(?P<system>[A-Za-z\-]+) \((?P<variant>[a-f0-9]+)\)',#'Unwanted-Program (004942d81)',
         'panda_online': '(?P<system>.+)/(?P<family>[A-Za-z]+)(?:\.(?P<variant>.*))?',#'PUP/MultiToolbar.A',
         'kaspersky_online': kav,
         'sophos': sophos,
         'mcafee_beta': mcafee,
         'trendmicro_beta': '(?P<system>.+)_(?P<family>[A-Za-z]+)(?:\.(?P<variant>.*))?',#'ADW_PULSOFT.SM',
         'command': '(?P<system>.+)/(?P<family>[A-Za-z]+)(?:\.(?P<variant>.*))?',#'W32/Virut.E.gen!Eldorado',
         'norman': '(?P<family>[A-Za-z]+)(?:\.(?P<variant>[A-Za-z0-9]+))?',
         'microsoft': '(?P<method>[A-Za-z0-9]+):(?P<system>[A-Za-z0-9]+)/(?P<family>[A-Za-z]+)(?:\.(?P<variant>[A-Za-z0-9]+))?',#'Virus:Win32/Virut.K',
         'quickheal': '(?P<system>[A-Za-z0-9\-]+)\.(?P<family>[A-Za-z]+)(?:\-(?P<variant>[A-Za-z0-9]+))?'
         }

#comile Regexs
for x,y in av_re.items():
    #print x,y
    av_re[x] = re.compile(y)

#apply the regexes and extract dict
def vt_opinions(vt):
    xys =  [(a.lower(),b) for a,b in vt]
    opinions = []
    for x,y in xys:

        if x in av_re:
            r = av_re[x].match(y)
            if r:
                family = r.groupdict().get('family')
                if family:
                    opinions.append((x,family.lower()))

    return opinions


def get_evalarrays():
    #DF = pd.read_csv('../data/families/families/VT_time.tsv',delimiter='\t')
    DF = pd.read_csv('../data/ucl/ucl/ucl.tsv',delimiter='\t')

    # Sort by time index
    DF.sort_values('time', inplace=True)

    # Split into first & last detection arrays
    DF_firsts = DF.drop_duplicates(subset='sha256', keep='first')
    DF_lasts = DF.drop_duplicates(subset='sha256', keep='last')

    # Get the set of VirusTotal AV vendors
    VT_vendors = set()
    for x in DF.verdicts:
        for y in vt_opinions(json.loads(x)):
            VT_vendors.add(y[0])

    # The list of RL vendors:
    RL_vendors = set(['ahnlab', 'antivir',
       'avast', 'avg', 'bitdefender', 'ca_av', 'clamav', 'command',
       'command_online', 'drweb', 'esetnod32', 'f_prot', 'fortinet', 'gdata',
       'ikarus', 'k7computing', 'kaspersky', 'kaspersky_online', 'mcafee',
       'mcafee_beta', 'mcafee_online', 'mcafeegwedition_online', 'microsoft',
       'norman', 'panda', 'panda_online', 'quickheal', 'rising',
       'rising_online', 'sophos', 'sophos_online', 'sunbelt', 'symantec',
       'symantec_beta', 'symantec_online', 'trendmicro', 'trendmicro_beta',
       'trendmicro_consumer', 'vba32', 'virusbuster'])

    # Now get the overlap vendor list
    vendors = list(VT_vendors.intersection(RL_vendors))

    # Build the joint array
    DF_joint = pd.merge(left=DF_firsts, right=DF_lasts, left_on='sha256', right_on='sha256')
    # Time dependencies
    DF_joint['time_x'] = pd.to_datetime(DF_joint['time_x'])
    DF_joint['time_y'] = pd.to_datetime(DF_joint['time_y'])
    DF_joint['lapse'] = (DF_joint['time_y'] - DF_joint['time_x']) / np.timedelta64(1, 'D')
    # Detection dependencies
    DF_joint['movement'] = DF_joint['positives_y'] - DF_joint['positives_x']
    # Add vendor columns
    for v in vendors:
        DF_joint[v] = 0

    # Get final labels
    # Current heuristic: Final detection with zero positives is clean, more than 4 is malware, rest uncertain
    # With > 1 week elapsed data will be able to refine the heuristic
    DF_joint['label'] = -1
    DF_joint.loc[DF_joint[DF_joint['positives_y'] > 4].index, 'label'] = 1
    DF_joint.loc[DF_joint[DF_joint['positives_y'] == 0].index, 'label'] = 0

    # Set the vendor verdicts (based on first detection only)
    for (index, row) in DF_joint.iterrows():
        for y in vt_opinions(json.loads(row['verdicts_x'])):
            if y[0] in vendors:
                DF_joint.loc[index, y[0]] = 1

    # Need our feature label mappings
    TG_labels = pd.read_csv('../data/Jan_CSV/TG_ioc_map.csv')

    # Now let's get features
    with open('../data/features.json/features.json') as f:
        TG_vals = json.load(f)

    # Create a data frame for the features
    TG_feats = pd.DataFrame(DF_joint['sha256'])
    for i in TG_labels['index']:
        TG_feats[str(i)] = 0

    for sha in TG_vals.keys():
        # Step 1: Get the SHA index, skip if not in our labels array
        sha_ind = TG_feats[TG_feats['sha256'] == sha].index
        if TG_vals[sha] == None:
            # Set the Nil feature
            TG_feats.loc[sha_ind, '413'] = 1
            continue
        for feat in TG_vals[sha]:
            # Step 2: Lookup the feature index, skip if no features triggered
            feat_ind = TG_labels[TG_labels['name'] == feat].index
            if len(feat_ind) == 0: continue
            # Step 3: Set the relevant feature to 1
            TG_feats.loc[sha_ind, str(feat_ind[0])] = 1

    return DF_joint, TG_feats