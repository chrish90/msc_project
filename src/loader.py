import numpy as np
import pandas as pd


def load_shas(return_list=True, keep='last', verbose=True):

    # Step 1: Get unique SHAs

    if verbose: print("Loading in RL info table (master file list)")

    RL_info = pd.read_csv('../data/Jan_CSV/RL_info.csv')

    # Drop the duplicates
    RL_info.drop_duplicates(subset=['sha256'], keep=keep, inplace=True)
    #lookup_shas = pd.Series(RL_info['sha256'].values,index=RL_info['index']).to_dict()

    # Process the scan dates
    RL_info[['first_scanned_on', 'last_scanned_on']] = RL_info[['first_scanned_on', 'last_scanned_on']].astype(int)
    RL_info['first_scanned_on'] = pd.to_datetime(RL_info['first_scanned_on'], unit='s')
    RL_info['last_scanned_on'] = pd.to_datetime(RL_info['last_scanned_on'], unit='s')
    RL_info['scan_length'] = (RL_info['last_scanned_on'] - RL_info['first_scanned_on'])  / np.timedelta64(1, 'D')

    # Produce the outputs
    id_df = RL_info[['index','sha256','scan_length']]
    #filter1 = RL_info['index'].values
    if verbose: print("Master file corpus size: ", len(id_df['index'].values))

    return id_df


def load_filetypefilter(filter_string, verbose=True):

    # Get filtered file type SHA list
    RL_magic = pd.read_csv('../data/Jan_CSV/RL_magic.csv')

    RL_magic['sample_type'] = RL_magic['sample_type'].str.lower()
    filter = RL_magic[RL_magic['sample_type'].str.contains(filter_string)]['index'].values
    if verbose: print("File corpus size fulfilling docstring %s: %d" % (filter_string, len(filter)))

    return filter


def get_AVhits(RL_final, verbose=True):

    filt = RL_final['index'].values

    if verbose: print("Loading in AV hits data")
    RL_hits = pd.read_csv('../data/Jan_CSV/RL_hits.csv')
    RL_hits.drop(['scanned_on','result'], axis=1, inplace=True)
    RL_hits = RL_hits[RL_hits['index'].isin(filt)]

    if verbose: print("Pivoting hits data")
    RL_hits['value'] = 1
    RL_hits = RL_hits.pivot(index='index', columns='scanner', values='value')
    RL_hits.fillna(0, inplace=True)

    if verbose: print("Merging data into ID array")
    RL_final = pd.merge(left=RL_final, right=RL_hits, how='left', left_on='index', right_index=True)

    if verbose: print("Subbing in zero values for *clean* files")
    #RL_final_columns = RL_final.columns.tolist()
    #TG_id = 'submission_id'
    #if TG_id in RL_final_columns: RL_final_columns.remove(TG_id)
    #RL_final[RL_final_columns].fillna(0, inplace=True)
    RL_final.fillna(0, inplace=True)

    print("Complete.")

    return RL_final


def get_features_ids(id_df, verbose=True):

    if verbose: print("Reading in features IDs")
    TG_subs = pd.read_csv('../data/Jan_CSV/TG_submissions_all.csv')

    # Drop unnecessary columns
    TG_subs.drop(['submitted_at','sample','vm'], axis=1, inplace=True)

    # Drop duplicate scans, retaining most recent scan
    TG_subs.drop_duplicates(subset=['sha256'], keep='last', inplace=True)

    #TG_subs = TG_subs[TG_subs['sha256'].isin(sha_list)]
    if verbose: print("ThreatGrid file corpus size: ", len(TG_subs))

    id_df = pd.merge(left=id_df, right=TG_subs, how='left', left_on='sha256', right_on='sha256')

    return id_df


def produce_labelsarray(filter_string, keep="last", features=True, verbose=True):

    # Get master SHA list
    RL_final = load_shas(verbose=verbose, keep=keep)

    # Get filter for MAGIC file type
    if filter_string:
        filter2 = load_filetypefilter(filter_string, verbose=verbose)

        filt = np.intersect1d(RL_final['index'].values, filter2, assume_unique=True)
        if verbose: print("Filter size: ", len(filt))

        RL_final = RL_final[RL_final['index'].isin(filt)]

    if verbose: print("ID Array size: ", len(RL_final))

    # Merge in feature IDs
    if features: RL_final = get_features_ids(RL_final, verbose=verbose)

    # Produce the final data array
    RL_final = get_AVhits(RL_final, verbose=verbose)

    return RL_final


def get_TGfeatures(sub_id_list):

    print("Loading ThreatGrid features data file")
    TG_ioc = pd.read_csv('../data/Jan_CSV/TG_ioc_indexed.csv')
    print("Feature array size: ", len(TG_ioc))

    if sub_id_list.any():
        print("Filtering to include only files in corpus")
        TG_ioc = TG_ioc[TG_ioc['submission_id'].isin(sub_id_list)]
        print("Feature array size: ", len(TG_ioc))

    print("Complete.")

    return TG_ioc
