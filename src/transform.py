import numpy as np
import pandas as pd
from scipy.sparse import csr_matrix

def get_datasets(RL_final, TG_ioc):

    # Kill files with no ThreatGrid data
    RL_final = RL_final[RL_final['submission_id'] != 0]
    RL_final.reset_index(drop=True, inplace=True)

    # Filter ThreatGrid files to those in RL
    RL_temp = pd.DataFrame(RL_final['submission_id'])
    RL_temp['RL_index'] = RL_temp.index
    TG_ioc = pd.merge(left=TG_ioc, right=RL_temp, how='left', left_on='submission_id', right_on='submission_id')

    # Construct sparse matrix
    feats = csr_matrix((np.ones(len(TG_ioc)),
                    (TG_ioc['RL_index'].values, TG_ioc['index'].values)), shape=(len(RL_final), 414))

    return RL_final, feats